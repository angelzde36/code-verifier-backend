import express, { Express, Request, Response } from 'express';
import dotenv from 'dotenv';


// configuration the .env file
dotenv.config();

// Create Express APP
const app: Express = express();
const port: string | number =  process.env.PORT || 8000 ;

// define the first Route of APP
app.get('/', (req: Request, res: Response) => {
    // Send Hello World
    res.send('WELCOME to API Resfult: Express + Nodemon + Jest + TS + Swagger + Mongoose')
});

app.get('/hello', (req: Request, res: Response) => {
    // Send Hello World
    res.send('Welcome to GET Route: ¡Hello!')
});

// execute APP and Listen Requests to PORT 
app.listen(port, () => console.log(`Expres SERVER: Running at http://localhost:${port}`))

