"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const dotenv_1 = __importDefault(require("dotenv"));
// configuration the .env file
dotenv_1.default.config();
// Create Express APP
const app = (0, express_1.default)();
const port = process.env.PORT || 8000;
// define the first Route of APP
app.get('/', (req, res) => {
    // Send Hello World
    res.send('WELCOME to API Resfult: Express + Nodemon + Jest + TS + Swagger + Mongoose');
});
app.get('/hello', (req, res) => {
    // Send Hello World
    res.send('Welcome to GET Route: ¡Hello!');
});
// execute APP and Listen Requests to PORT 
app.listen(port, () => console.log(`Expres SERVER: Running at http://localhost:${port}`));
//# sourceMappingURL=index.js.map